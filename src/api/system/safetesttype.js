import request from '@/utils/request'
import {
  encrypt
} from '@/utils/rsaEncrypt'
// 树形结构数据
export function getquery(data) {
  return request({
    url: 'api/safeTestType/query',
    method: 'get',
    params:data
  })
}
// 右侧列表详情
export function getqueryById(data) {
  return request({
    url: 'api/safeTestItem/queryById',
    method: 'post',
    params: data
  })
}
//风险规则列表页
export function getcase(data) {
  return request({
    url: 'api/safeTestCase',
    method: 'get',
    params:data
  })
}
export default {
  getquery,
  getqueryById,
  getcase

}