// 需求列表页接口
import request from '@/utils/request'
import {
  encrypt
} from '@/utils/rsaEncrypt'
// 需求列表
export function getmainquery(data) {
  return request({
    url: 'api/safeRequirement/query',
    method: 'get',
    data
  })
}
// 智能推荐按钮 (123)
export function getoneType(data) {
  return request({
    url: 'api/safeTestType/recommend',
    method: 'get',
    params: data
  })
}
// 系统列表
export function getsafeSystem(data) {
  return request({
    url: 'api/safeSystem/query',
    method: 'get',
    data
  })
}
// 标签管理
export function getlabel(data) {
  return request({
    url: 'api/safeTestType/tags',
    method: 'get',
    data
  })
}
// 新增需求
export function postadd(data, params1) {
  return request({
    url: 'api/safeRequirement/create',
    method: 'post',
    data,
    params: {
      systemId: params1
    }

  })
}
// 新增系统
export function postcreate(data) {
  return request({
    url: 'api/safeSystem/create',
    method: 'post',
    data
  })
}
// 修改系统
export function putedit(data) {
  return request({
    url: 'api/safeSystem/edit',
    method: 'put',
    data
  })
}
// 修改需求
export function putsafe(data) {
  return request({
    url:'api/safeRequirement/edit',
    method: 'put',
    data
  })
}

export default {
  getmainquery,
  getoneType,
  getsafeSystem,
  getlabel,
  postadd,
  postcreate,
  putedit,
  putsafe
}
