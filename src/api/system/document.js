import request from '@/utils/request'
import {
  encrypt
} from '@/utils/rsaEncrypt'
// 安全报告列表页
export function getdocument(data) {
  console.log(data)
  return request({
    url: 'api/document',
    method: 'get',
    params:data
  })
}
// 自定义获取列表文件名称
export function getlist(parmas) {
 return request({
    params: {
      docType: '0'
    },
    url: 'api/document/all',
    method: 'get',
    parmas
  })
}
// 右侧表格
export function gettablelist(data) {
  console.log(data,'documentjs')
  return request({
    url: 'api/document/tableList',
    method: 'get',
    params:data

  })
}
//生成自定义文件
export function getgenerate(data) {
  console.log(data,'documentjs')
  return request({
    url: 'api/document/create',
    method: 'post',
    params:data

  })
}
// 安全报告上传文件
// export function getupload(data) {
//   const params = {
//     name: data.filename,
//     file: data.file,
//     isModel: false
//   }
//   console.log(data)
//   return request({
//     url: 'api/document',
//     method: 'post',
//     params
//   })
// }
// 安全报告下载文件
export function getdownload(parmas) {
  // const params = {
  //   path: data.path
  // }
  return request({
    url: 'api/document/download',
    method: 'post',
    parmas
    
  })
}

export function del(ids) {
  return request({
    url: 'api/users',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/users',
    method: 'post',
    data
  })
}

export function editUser(data) {
  return request({
    url: 'api/users/center',
    method: 'put',
    data
  })
}

export function updatePass(user) {
  const data = {
    oldPass: encrypt(user.oldPass),
    newPass: encrypt(user.newPass)
  }
  return request({
    url: 'api/users/updatePass/',
    method: 'post',
    data
  })
}

export function updateEmail(form) {
  const data = {
    password: encrypt(form.pass),
    email: form.email
  }
  return request({
    url: 'api/users/updateEmail/' + form.code,
    method: 'post',
    data
  })
}

export default {
  gettablelist,
  edit,
  del,
  getdocument,
  getlist,
  getgenerate,
  getdownload

}
